import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { AuthProvider } from "./components/AuthContext";
import PrivateRoute from "./components/PrivateRoute";
import Login from "./components/Login";
import Dashboard from "./components/Dashboard";
import Header from "./components/Header";
import { TodoProvider } from "./components/TodoContext";

const App: React.FC = () => {
  return (
    <Router>
      {/* Wrapping with AuthProvider to get access to context of it */}
      <AuthProvider>
        <Header />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<PrivateRoute />}>
            <Route
              path="/dashboard"
              element={
                <TodoProvider>
                  {/* Wrapping with TodoProvider to get access to context of it */}
                  <Dashboard />
                </TodoProvider>
              }
            />
          </Route>
          <Route path="*" element={<h1>404 Not Found</h1>} />
        </Routes>
      </AuthProvider>
    </Router>
  );
};

export default App;
