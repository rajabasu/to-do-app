import React, { createContext, useEffect, useState, ReactNode } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useNavigate } from 'react-router-dom';

interface AuthProviderProps {
  children: ReactNode;
}

interface AuthContextProps {
  user: User | null;
  login: (username: string, password: string) => void;
  logout: () => void;
}

interface User {
  id: string;
  username: string;
}

// AuthContext
const AuthContext = createContext<AuthContextProps>({
  user: null,
  /* eslint-disable @typescript-eslint/no-empty-function */
  login: () => {},
  logout: () => {},
  /* eslint-enable @typescript-eslint/no-empty-function */
});

// AuthContext provider
const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);
  const navigate = useNavigate();

  // This is used if user is already logged in and the page is refresh the user details are stored in localStorage again
  useEffect(() => {
    const storedUser = localStorage.getItem('user');

    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  const login = (username: string, password: string) => {
    if (username === 'admin' && password === 'password') {
      const newUser: User = {
        id: uuidv4(),
        username,
      };

      localStorage.setItem('user', JSON.stringify(newUser));
      setUser(newUser);

      navigate('/dashboard');
    }
  };

  const logout = () => {
    setUser(null);
    localStorage.removeItem('user');
    navigate('/');
  };

  return (
    <AuthContext.Provider value={{ user, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthProvider };
