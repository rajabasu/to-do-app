import React, { useContext, useState, ChangeEvent } from 'react';
import {
  Container,
  Title,
  Button,
  Grid,
  Text,
  Box,
  createStyles,
  Accordion,
  Divider,
} from '@mantine/core';
import { IconChevronDown, IconCircleCheck } from '@tabler/icons-react';
import { AuthContext } from './AuthContext';
import { useTodoContext, Todo } from './TodoContext';
import Modal from './Modal';

const useStyles = createStyles((theme) => ({
  active: {
    color: theme.colors.blue[5],
  },
  todoDiv: {
    borderTop: `1px solid ${theme.colors.gray[3]}`,
    marginLeft: 40,
    paddingTop: 10,
    paddingBottom: 10,
  },
  todoDisabled: {
    opacity: '1 !important',
    cursor: 'default !important',
    backgroundColor: theme.colors.gray[0],
  },
}));

interface todoListProps {
  newTodo: string;
  subTodo: string;
}

const Dashboard: React.FC = () => {
  const { classes } = useStyles();
  const { user } = useContext(AuthContext);
  const { todos, addTodo, addNestedTodo } = useTodoContext();
  const [todoList, setTodoList] = useState<todoListProps>({
    newTodo: '',
    subTodo: '',
  });
  const [selectedTodo, setSelectedTodo] = useState<string>('');
  const [addNewTodoOpenModal, setAddNewTodoOpenModal] =
    useState<boolean>(false);
  const [addSubTodoOpenModal, setAddSubTodoOpenModal] =
    useState<boolean>(false);

  const handleAddNewTodoModal = () => {
    setAddNewTodoOpenModal(!addNewTodoOpenModal);
  };

  const handleAddSubTodoModal = () => {
    setAddSubTodoOpenModal(!addSubTodoOpenModal);
  };

  // For adding new todo
  const handleAddTodo = () => {
    if (todoList.newTodo.trim() !== '') {
      const todo: Todo = {
        id: Math.random().toString(),
        title: todoList.newTodo,
        subTasks: [],
      };
      addTodo(todo);
      setTodoList((prevState) => ({
        ...prevState,
        newTodo: '',
      }));
      handleAddNewTodoModal();
    }
  };

  // For added nested todo to existing todo
  const handleAddNestedTodo = () => {
    if (todoList.subTodo.trim() !== '') {
      const todo: Todo = {
        id: Math.random().toString(),
        title: todoList.subTodo,
        subTasks: [],
      };
      addNestedTodo(selectedTodo, todo);
      setTodoList((prevState) => ({
        ...prevState,
        subTodo: '',
      }));
      setSelectedTodo('');
      handleAddSubTodoModal();
    }
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setTodoList((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSelected = ({ id }: { id: string }) => {
    setSelectedTodo(id);
  };

  const handleClearSelection = () => {
    setSelectedTodo('');
  };

  return (
    <Container>
      <Title align='center' mb={20}>
        Welcome, {user?.username}!
      </Title>

      <Divider my={50} />

      <Grid align='center' my={30}>
        <Grid.Col span={8} sm={4}>
          <Title order={2}>Add New Todo</Title>
        </Grid.Col>
        <Grid.Col span={4} sm={2} px={0}>
          <Button size='sm' fullWidth onClick={handleAddNewTodoModal}>
            Add
          </Button>
        </Grid.Col>
      </Grid>

      <Grid align='center'>
        <Grid.Col span={8} sm={4}>
          <Title order={2}>My Todos</Title>
        </Grid.Col>

        <Grid.Col span={4} sm={2} px={0}>
          <Button
            size='sm'
            fullWidth
            onClick={handleAddSubTodoModal}
            disabled={!selectedTodo}
          >
            Add Subtask
          </Button>
        </Grid.Col>
      </Grid>

      <Divider my={20} />

      {todos.length > 0 && (
        <Grid justify='end' align='center'>
          <Grid.Col span={5} sm={2}>
            <Button
              size='sm'
              fullWidth
              onClick={handleClearSelection}
              disabled={!selectedTodo}
            >
              Clear Selection
            </Button>
          </Grid.Col>
        </Grid>
      )}

      <Accordion variant='separated' my={20}>
        {todos.map((todo) => (
          <Accordion.Item key={todo.id} value={todo.id}>
            <Accordion.Control
              icon={
                <IconCircleCheck
                  className={`${selectedTodo === todo.id && classes.active}`}
                />
              }
              chevron={
                !todo.subTasks.length && <IconChevronDown color='transparent' />
              }
              className={`${!todo.subTasks.length && classes.todoDisabled}`}
              onClick={() => handleSelected({ id: todo.id })}
            >
              {todo.title}
            </Accordion.Control>
            {todo.subTasks.length > 0 && (
              <Accordion.Panel>
                {todo.subTasks.map((subTodo) => (
                  <Box key={subTodo.id} className={classes.todoDiv}>
                    <Text size='md'>{subTodo.title}</Text>
                  </Box>
                ))}
              </Accordion.Panel>
            )}
          </Accordion.Item>
        ))}
      </Accordion>

      <Modal
        opened={addNewTodoOpenModal}
        onClose={handleAddNewTodoModal}
        title={<Title>Add new todo</Title>}
        name='newTodo'
        placeholder='Add new todo'
        onChange={handleChange}
        onClick={handleAddTodo}
      />

      <Modal
        opened={addSubTodoOpenModal}
        onClose={handleAddSubTodoModal}
        title={<Title>Add new sub-todo</Title>}
        name='subTodo'
        placeholder='Add new sub-todo'
        onChange={handleChange}
        onClick={handleAddNestedTodo}
      />
    </Container>
  );
};

export default Dashboard;
