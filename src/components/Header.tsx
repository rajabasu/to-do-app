import {
  createStyles,
  Header as MantineHeader,
  Group,
  Button,
  Text,
  Box,
  Burger,
  Drawer,
  ScrollArea,
  rem,
  Container,
  Menu,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useNavigate } from "react-router-dom";
import { IconLogout } from "@tabler/icons-react";
import { AuthContext } from "./AuthContext";
import { useContext } from "react";

const useStyles = createStyles((theme) => ({
  hiddenMobile: {
    [theme.fn.smallerThan("sm")]: {
      display: "none",
    },
  },

  hiddenDesktop: {
    [theme.fn.largerThan("sm")]: {
      display: "none",
    },
  },
}));

const Header = () => {
  const [drawerOpened, { toggle: toggleDrawer, close: closeDrawer }] =
    useDisclosure(false);
  const { classes } = useStyles();
  const { user, logout } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    closeDrawer();
    logout();
    navigate("/");
  };

  return (
    <Box pb={20}>
      <MantineHeader
        height={60}
        style={{ display: "flex", alignItems: "center" }}
      >
        <Container style={{ width: "100%" }}>
          <Group position="apart" sx={{ height: "100%" }}>
            <Text fw={700}>To-Do-App</Text>

            {user && (
              <>
                <Group className={classes.hiddenMobile}>
                  <Menu
                    shadow="md"
                    variant="outlined"
                    position="bottom-end"
                    withArrow
                  >
                    <Menu.Target>
                      <Button variant="outline">
                        {user?.username.toUpperCase()}
                      </Button>
                    </Menu.Target>
                    <Menu.Dropdown>
                      <Menu.Item
                        color="red"
                        icon={<IconLogout size={14} />}
                        onClick={handleLogout}
                      >
                        Logout
                      </Menu.Item>
                    </Menu.Dropdown>
                  </Menu>
                </Group>
                <Burger
                  opened={drawerOpened}
                  onClick={toggleDrawer}
                  className={classes.hiddenDesktop}
                />
              </>
            )}
          </Group>
        </Container>
      </MantineHeader>

      <Drawer
        opened={drawerOpened}
        onClose={closeDrawer}
        size="100%"
        padding="md"
        title={<Text fw={700}>{user?.username.toUpperCase()}</Text>}
        className={classes.hiddenDesktop}
        zIndex={1000000}
      >
        <ScrollArea h={`calc(100vh - ${rem(60)})`} mx="-md">
          <Group position="center" grow pb="xl" px="md">
            <Button
              color="red"
              leftIcon={<IconLogout />}
              variant="outline"
              onClick={handleLogout}
            >
              Logout
            </Button>
          </Group>
        </ScrollArea>
      </Drawer>
    </Box>
  );
};

export default Header;
