import React, { useContext, useState, ChangeEvent } from 'react';
import { AuthContext } from './AuthContext';
import {
  Container,
  Title,
  Paper,
  TextInput,
  PasswordInput,
  Button,
} from '@mantine/core';

interface loginCredentialsProps {
  username: string;
  password: string;
}

interface loginErrorCredentialsProps {
  username: string;
  password: string;
}

const Login: React.FC = () => {
  const [loginCredentials, setLoginCredentials] =
    useState<loginCredentialsProps>({
      username: '',
      password: '',
    });
  const [loginErrorCredentials, setLoginErrorCredentials] =
    useState<loginErrorCredentialsProps>({
      username: '',
      password: '',
    });
  const { login } = useContext(AuthContext);

  const handleLogin = () => {
    // Login error handling
    if (!loginCredentials.username) {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        username: 'Please enter username',
      }));
      setTimeout(() => {
        setLoginErrorCredentials((prevState) => ({
          ...prevState,
          username: '',
        }));
      }, 5000);
    }

    if (!loginCredentials.password) {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        password: 'Please enter password',
      }));
      setTimeout(() => {
        setLoginErrorCredentials((prevState) => ({
          ...prevState,
          password: '',
        }));
      }, 5000);
    }

    if (loginCredentials.username && loginCredentials.username !== 'admin') {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        username: 'Wrong username',
      }));
      setTimeout(() => {
        setLoginErrorCredentials((prevState) => ({
          ...prevState,
          username: '',
        }));
      }, 5000);
    }

    if (loginCredentials.password && loginCredentials.password !== 'password') {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        password: 'Wrong password',
      }));
      setTimeout(() => {
        setLoginErrorCredentials((prevState) => ({
          ...prevState,
          password: '',
        }));
      }, 5000);

      return;
    }

    // If no error calling login function
    login(loginCredentials.username, loginCredentials.password);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (name === 'username') {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        username: '',
      }));
    }
    if (name === 'password') {
      setLoginErrorCredentials((prevState) => ({
        ...prevState,
        password: '',
      }));
    }

    setLoginCredentials((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <Container>
      <Container size={420} my={40}>
        <Title
          align='center'
          sx={(theme: { fontFamily: unknown }) => ({
            fontFamily: `Greycliff CF, ${theme.fontFamily}`,
            fontWeight: 900,
          })}
        >
          Log in!
        </Title>

        <Paper withBorder shadow='md' p={30} mt={30} radius='md'>
          <TextInput
            label='User name'
            name='username'
            placeholder='User name'
            required
            onChange={handleChange}
            value={loginCredentials.username}
            error={loginErrorCredentials.username}
          />

          <PasswordInput
            label='Password'
            name='password'
            placeholder='Your password'
            required
            mt='md'
            onChange={handleChange}
            value={loginCredentials.password}
            error={loginErrorCredentials.password}
          />

          <Button fullWidth mt='xl' onClick={handleLogin}>
            Sign in
          </Button>
        </Paper>
      </Container>
    </Container>
  );
};

export default Login;
