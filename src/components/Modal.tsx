import React, { ChangeEvent, MouseEvent, useState } from "react";
import {
  Button,
  Modal as MantineModal,
  ModalProps as MantineModalProps,
  TextInput,
  Title,
  Grid,
  CloseButton,
} from "@mantine/core";

interface ModalProps extends Omit<MantineModalProps, "onClick" | "onChange"> {
  name: string;
  placeholder?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onClick: (event: MouseEvent<HTMLButtonElement>) => void;
}

const Modal: React.FC<ModalProps> = ({
  opened,
  onClose,
  title,
  name,
  placeholder,
  onChange,
  onClick,
}) => {
  const [value, setValue] = useState<string>("");
  const [valueError, setValueError] = useState<string>("");

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValueError("");
    const { value } = e.target;
    setValue(value);
    if (onChange) {
      onChange(e);
    }
  };

  const handleClose = () => {
    onClose();
    setValue("");
  };

  const handleAdd = (e: MouseEvent<HTMLButtonElement>) => {
    if (!value) {
      setValueError(`Please enter something`);

      return;
    }

    onClick(e);
    setValue("");
    setValueError("");
  };

  return (
    <MantineModal opened={opened} onClose={handleClose} withCloseButton={false}>
      <Grid justify="space-between" align="center">
        <Grid.Col span={11}>
          <Title>{title}</Title>
        </Grid.Col>
        <Grid.Col span={1} p={0}>
          <CloseButton size="lg" variant="transparent" onClick={handleClose} />
        </Grid.Col>
      </Grid>
      <TextInput
        name={name}
        placeholder={placeholder}
        size="md"
        mt={20}
        onChange={handleChange}
        value={value}
        error={valueError}
      />
      <Button size="md" fullWidth my={20} onClick={handleAdd}>
        Add
      </Button>
    </MantineModal>
  );
};
export default Modal;
