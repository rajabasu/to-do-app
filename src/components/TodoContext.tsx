import React, { ReactNode, createContext, useContext, useState } from 'react';
import { AuthContext } from './AuthContext';
import { useNavigate } from 'react-router-dom';

export interface Todo {
  id: string;
  title: string;
  subTasks: Todo[];
}

interface TodoContextProps {
  todos: Todo[];
  addTodo: (todo: Todo) => void;
  addNestedTodo: (parentId: string, todo: Todo) => void;
}

interface TodoProviderProps {
  children: ReactNode;
}

const TodoContext = createContext<TodoContextProps>({
  todos: [],
  /* eslint-disable @typescript-eslint/no-empty-function */
  addTodo: () => {},
  addNestedTodo: () => {},
  /* eslint-enable @typescript-eslint/no-empty-function */
});

export const TodoProvider: React.FC<TodoProviderProps> = ({ children }) => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const navigate = useNavigate();
  const { logout } = useContext(AuthContext);
  const user = localStorage.getItem('user');

  const handleLogout = () => {
    logout();
    navigate('/');
  };

  const addTodo = (todo: Todo) => {
    // Logout if user is not logged in
    if (!user) {
      handleLogout();
      return;
    }

    setTodos((prevTodos) => [...prevTodos, todo]);
  };

  const addNestedTodo = (parentId: string, todo: Todo) => {
    // Logout if user is not logged in
    if (!user) {
      handleLogout();
      return;
    }
    setTodos((prevTodos) => {
      const updatedTodos = [...prevTodos];
      const parentTodo = updatedTodos.find((t) => t.id === parentId);
      if (parentTodo) {
        parentTodo.subTasks.push(todo);
      }
      return updatedTodos;
    });
  };

  return (
    <TodoContext.Provider value={{ todos, addTodo, addNestedTodo }}>
      {children}
    </TodoContext.Provider>
  );
};

export const useTodoContext = (): TodoContextProps => useContext(TodoContext);
